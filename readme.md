# NFM KiCad Parts Library

An Open Source parts library for KiCad by NFM. If you like this resource, please [visit our website](http://nfmtech.com) and spread the word!

Written by [Darian Cabot](http://dariancabot.com) for [NFM Tech](http://nfmtech.com). See license details below.

## KiCad version

The current version of KiCad used to create and test this parts library is 4.0.4-stable.

Your mileage may vary for other KiCad versions.

## Library stability

We understand that for a parts library to be used in production, it needs to be stable, so changes don't cause issues for your designs in future.

To address this concern, our library uses a strict release method. Each major update to the library is in its own version folder (i.e. ``v01``, ``v02``). This folder name is suffixed with a descriptor, either ``development`` or ``stable``. If a version is marked ``development``, it is  actively being modified and should be avoided for production designs! Once development has finished on a version, it is then changed to ``stable`` and will not be modified in future. Further development will be in a new version number marked ``development``.

For example:

```
nfm-kicad-parts-library/
  ./v01-stable/       <-- Safe for production.
  ./v02-development/  <-- Not safe!
```

So, to ensure your production designs aren't affected:

1. Only use parts in the *stable* version folders for production designs.
2. To be extra safe, copy a version of this library from your git folder to your project folder and do not modify it.

Also, check the readme in each version directory for any specific notes including intended KiCad version and errata.

## License

This library is released under the [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.

Copyright (c) 2016 NFM Tech. All rights reserved.

You are free to:

- **Share** — copy and redistribute the material in any medium or format
- **Adapt** — remix, transform, and build upon the material for any purpose, even commercially.

Under the following terms:

- **Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
- **ShareAlike** — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original. 
